package fr.ap.test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Main test class.
 */

public class Main {

    private static final String SHUTDOWN_COMMENTARY_MESSAGE = "\"Scheduled-Shutdown va arrêter votre ordinateur.\"";
    private static final String ERROR_MESSAGE = "Error : Windows command not sent | ";

    public static void main(String[] args) {

        int seconds = 3600;

        try {
            Process process = Runtime.getRuntime().exec("shutdown -s -t " + seconds + " -c " + SHUTDOWN_COMMENTARY_MESSAGE + " -f");

            System.out.println("\nCommande passée : shutdown -r -t " + seconds + " -c " + SHUTDOWN_COMMENTARY_MESSAGE + " -f\n");

            InputStream inputStream = process.getInputStream();
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
            System.out.println("EVENT : Windows command sent : shutdown -s -t " + seconds);
        } catch (IOException error) {
            System.out.println(ERROR_MESSAGE + error);
        }
    }
}
