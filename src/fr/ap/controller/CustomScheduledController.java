package fr.ap.controller;

import fr.ap.application.Main;
import fr.ap.application.Reference;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class CustomScheduledController {


    private Main main;
    @FXML
    private TextField hourTextField;
    @FXML
    private TextField minuteTextField;
    @FXML
    private TextField secondTextField;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    void programOnAction(ActionEvent event) throws IOException {

        int scheduledTime = scheduledTimeConvert();

        if (scheduledTime < 60 || scheduledTime > 315360000) {
            if (scheduledTime == 0) {
                if (scheduledTime == 0) {
                    showErrorDialog("Valeur incorrecte détectée.", "Veuillez entrer une valeur valide ou supérieure à zéro.");
                    setEmptyValueTextField();
                }
            }
            if (scheduledTime < 60 && scheduledTime != 0) {
                showErrorDialog("La programmation ne peut être inférieur à une minute.", "Veuillez entrer une valeur supérieure.");
                setEmptyValueTextField();
            }
        } else showConfirmationDialog(scheduledTime);
    }

    private void setEmptyValueTextField() {
        secondTextField.setText("");
        minuteTextField.setText("");
        hourTextField.setText("");
    }

    private void showConfirmationDialog(int timeScheduledConverted) throws IOException {
        int displayTime = 0;
        String displayFormat = "";

        main.showProgramConfirmation(displayTime, displayFormat, timeScheduledConverted);
    }

    private void showErrorDialog(String headerText, String contentText) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        DialogPane dialogPane = errorAlert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        Stage stage = (Stage) errorAlert.getDialogPane().getScene().getWindow();

        Image image = new Image(getClass().getResourceAsStream("/img/logo/delete_red_styledcross.png"));
        ImageView imageView = new ImageView(image);

        imageView.setFitHeight(64);
        imageView.setFitWidth(64);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(7).png")));
        errorAlert.setGraphic(imageView);
        errorAlert.getButtonTypes().remove(ButtonType.CANCEL);
        errorAlert.setTitle("Programmation impossible");
        errorAlert.setHeaderText(headerText);
        errorAlert.setContentText(contentText);
        errorAlert.showAndWait();
    }

    private int scheduledTimeConvert() {
        int hour = 0;
        int minute = 0;
        int second = 0;

        try {
            hour = Integer.parseInt(hourTextField.getText());
        } catch (NumberFormatException error) {
            System.out.println("EVENT : CustomScheduledController class : invalid format input (hour). | " + error);
            if (hourTextField.getText().equals("")) hour = 0;
            System.out.println("PATCH : int hour value affected to '0'");
        }
        try {
            minute = Integer.parseInt(minuteTextField.getText());
        } catch (NumberFormatException error) {
            System.out.println("EVENT : CustomScheduledController class : invalid format input (minute). | " + error);
            if (minuteTextField.getText().equals("")) minute = 0;
            System.out.println("PATCH : int minute value affected to '0'");
        }
        try {
            second = Integer.parseInt(secondTextField.getText());
        } catch (NumberFormatException error) {
            System.out.println("EVENT : CustomScheduledController class : invalid format input (second). | " + error);
            if (secondTextField.getText().equals("")) second = 0;
            System.out.println("PATCH : int second value affected to '0'");
        }

        hour = (hour * 60) * 60;
        minute *= 60;

        return hour + minute + second;
    }

    @FXML
    void cancelOnAction(ActionEvent event) {
        main.getCustomScheduledStage().close();
    }

    @FXML
    void initialize() {
        assert hourTextField != null : "fx:id=\"hourTextField\" was not injected: check your FXML file 'CustomScheduled.fxml'.";
        assert minuteTextField != null : "fx:id=\"minuteTextField\" was not injected: check your FXML file 'CustomScheduled.fxml'.";
        assert secondTextField != null : "fx:id=\"secondTextField\" was not injected: check your FXML file 'CustomScheduled.fxml'.";
    }
}
