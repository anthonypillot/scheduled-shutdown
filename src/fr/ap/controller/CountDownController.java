package fr.ap.controller;

import fr.ap.application.Reference;
import fr.ap.application.Main;
import fr.ap.service.WindowsCommandPrompt;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Open a Window and set a countdown with cancel shutdown and close countdown buttons.
 * <p></p>
 * <p><code>{@link #launchCountDown(int)} (setup the countdown)</code></p>
 * <p></p>
 * <p>@see <a href="https://stackoverflow.com/questions/14393423/how-to-make-a-countdown-timer-in-java">How to make a CountDown.</a></p>
 */
public class CountDownController {

    private Main main;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private Label headLabel;
    @FXML
    private Label countDownLabel;
    private Timer timer;
    private int interval;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    void backOnAction(ActionEvent event) {
        main.getCountDownStage().hide();
        main.getPrimaryStage().show();
    }

    @FXML
    void cancelOnAction(ActionEvent event) {
        Alert confirmationWindow = createAlertWindow("/img/chrono/stopwatch.png", "Annulation arrêt programmé", "Confirmez-vous l'annulation de l'arrêt programmé de cet ordinateur ?", "Retour au menu principal.");

        Optional<ButtonType> response = confirmationWindow.showAndWait();
        if (response.isPresent() && response.get() == ButtonType.OK) {
            timer.cancel();
            WindowsCommandPrompt.cancel();
            main.getMainController().disableSchedulingButton(false);
            main.getMainController().disableCancelButton(true);
            main.getMainController().setScheduled(false);
            main.getCountDownStage().close();
            main.getPrimaryStage().show();
        }
    }

    private Alert createAlertWindow(String imgPath, String title, String headerText, String contentText) {
        Alert alertWindow = new Alert(Alert.AlertType.CONFIRMATION);
        DialogPane dialogPane = alertWindow.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        Stage stage = (Stage) alertWindow.getDialogPane().getScene().getWindow();

        Image image = new Image(getClass().getResourceAsStream(imgPath));
        ImageView imageView = new ImageView(image);

        imageView.setFitHeight(64);
        imageView.setFitWidth(64);
        stage.getIcons().add(image);
        alertWindow.setGraphic(imageView);
        alertWindow.setTitle(title);
        alertWindow.setHeaderText(headerText);
        alertWindow.setContentText(contentText);
        return alertWindow;
    }

    public void displayConfirmationWindow(WindowEvent windowEvent) {
        Alert confirmationWindow = createAlertWindow("/img/chrono/stopwatch.png", "Annulation arrêt programmé", "Confirmez-vous l'annulation de l'arrêt programmé de cet ordinateur ?", "Retour au menu principal.");

        Optional<ButtonType> response = confirmationWindow.showAndWait();
        if (response.isPresent() && response.get() == ButtonType.OK) {
            timer.cancel();
            WindowsCommandPrompt.cancel();
            main.getMainController().disableSchedulingButton(false);
            main.getMainController().disableCancelButton(true);
            main.getMainController().setScheduled(false);
            main.getCountDownStage().close();
            main.getPrimaryStage().show();
        } else confirmationWindow.close();
        windowEvent.consume();
    }

    /**
     * Starting a countdown with a <code>timeScheduled(int)</code> which use an upper caught time in second.
     *
     * @param timeScheduled time caught from the MainController or CustomScheduledController.
     */

    public void launchCountDown(int timeScheduled) {

        System.out.println("EVENT : Input countdown of [" + timeScheduledToString(timeScheduled) + "] (format : hh:mm:ss)");

        int delay = 1000;
        int period = 1000;

        timer = new Timer();
        interval = timeScheduled;

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                Platform.setImplicitExit(false);
                Platform.runLater(() -> countDownLabel.setText(timeScheduledToString(setInterval())));
                Platform.runLater(() -> main.getCountDownStage().setTitle("Décompte : " + timeScheduledToString(interval)));

                System.out.println("EVENT : Time remaining -> [" + timeScheduledToString(interval) + "]");

            }
        }, delay, period);
    }

    public String timeScheduledToString(int timeScheduled) {
        int hours = timeScheduled / 3600;
        int minutes = (timeScheduled % 3600) / 60;
        int seconds = timeScheduled % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * If the interval(timeScheduled) == 1 , the computer will stop and the platform will shutdown too.
     * <p></p>
     * <p>Checking if the Hibernate Radio button is selected, if it is, launch WindowsCommand.</p>
     * <p>Exiting the application before stopping : <code>Platform.exit();</code></p>
     */
    private int setInterval() {
        if (interval == 1) {
            timer.cancel();
            if (main.getMainController().getHibernateRadioButton().isSelected()) {
                WindowsCommandPrompt.hibernate();
            }
            Platform.exit();
        }
        return --interval;
    }

    public Label getHeadLabel() {
        return headLabel;
    }

    public Label getCountDownLabel() {
        return countDownLabel;
    }

    Timer getTimer() {
        return timer;
    }

    @FXML
    void initialize() {
        assert headLabel != null : "fx:id=\"headLabel\" was not injected: check your FXML file 'CountDown.fxml'.";
        assert countDownLabel != null : "fx:id=\"countDownLabel\" was not injected: check your FXML file 'CountDown.fxml'.";
    }
}
