package fr.ap.controller;

import fr.ap.application.Main;
import fr.ap.util.FileReaderConverter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class LicenseController {

    private Main main;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private TextArea licenseTextArea;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    void closeOnAction(ActionEvent event) {
        main.getLicenseStage().close();
    }

    @FXML
    void initialize() {
        assert licenseTextArea != null : "fx:id=\"licenseTextArea\" was not injected: check your FXML file 'License.fxml'.";

        licenseTextArea.setText(FileReaderConverter.convertFileToString("LICENSE"));
    }
}
