package fr.ap.controller;

import fr.ap.application.Main;
import fr.ap.service.WindowsCommandPrompt;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ProgramConfirmationController {

    private Main main;

    //TODO do something with those variables ?
    private int displayTime;
    private String displayFormat;
    private int scheduledTime;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private Label headerLabel;
    @FXML
    private Label contentLabel;
    @FXML
    private CheckBox checkBox;
    @FXML
    private Button confirmationButton;
    @FXML
    private Button cancelButton;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    void confirmationOnAction(ActionEvent event) throws IOException {
        System.out.println("EVENT : Confirmation button pressed in ProgramConfirmation window");
        if (checkBox.isSelected()) {
            System.out.println("EVENT : Countdown checkbox is selected, launching Countdown window");
            programLaunch();
            main.getPrimaryStage().hide();
            main.getCountDownStage().show();
        } else {
            System.out.println("EVENT : Countdown checkbox is not selected");
            programLaunch();
        }
    }

    private void programLaunch() throws IOException {
        if (main.getMainController().getShutdownRadioButton().isSelected()) {
            WindowsCommandPrompt.shutdown(scheduledTime);
        }
        if (main.getMainController().getRebootRadioButton().isSelected()) {
            WindowsCommandPrompt.reboot(scheduledTime);
        }
        main.getMainController().disableSchedulingButton(true);
        main.getMainController().disableCancelButton(false);
        main.getMainController().setScheduled(true);
        main.showCountDownWindow(scheduledTime);
        main.getProgramConfirmationStage().close();

        if (main.getCustomScheduledStage() != null) {
            main.getCustomScheduledStage().close();
            System.out.println("EVENT : CustomScheduled window closed");
        }
    }

    @FXML
    void cancelOnAction(ActionEvent event) {
        main.getProgramConfirmationStage().close();
    }

    public Label getHeaderLabel() {
        return headerLabel;
    }

    public Label getContentLabel() {
        return contentLabel;
    }

    public void initializeVariables(int displayTime, String displayFormat, int timeCommandInSeconds) {
        this.displayTime = displayTime;
        this.displayFormat = displayFormat;
        this.scheduledTime = timeCommandInSeconds;
    }

    @FXML
    void initialize() {
        assert headerLabel != null : "fx:id=\"headerLabel\" was not injected: check your FXML file 'ProgramConfirmation.fxml'.";
        assert contentLabel != null : "fx:id=\"contentLabel\" was not injected: check your FXML file 'ProgramConfirmation.fxml'.";
        assert checkBox != null : "fx:id=\"checkBox\" was not injected: check your FXML file 'ProgramConfirmation.fxml'.";

        checkBox.setSelected(true);
    }
}
