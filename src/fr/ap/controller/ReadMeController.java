package fr.ap.controller;

import fr.ap.application.Main;
import fr.ap.util.FileReaderConverter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class ReadMeController {

    private Main main;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private TextArea readmeTextArea;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    void closeOnAction(ActionEvent event) {
        main.getReadmeStage().close();
    }

    @FXML
    void initialize() {
        assert readmeTextArea != null : "fx:id=\"readmeTextArea\" was not injected: check your FXML file 'License.fxml'.";
        
        readmeTextArea.setText(FileReaderConverter.convertFileToString("README.txt"));
    }
}
