package fr.ap.controller;

import fr.ap.application.Main;
import fr.ap.application.Reference;
import fr.ap.service.WindowsCommandPrompt;
import fr.ap.util.ExternalResource;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController {

    private Main main = new Main();
    private boolean isScheduled;

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Label label4;
    @FXML
    private MenuItem fileNewCustom;
    @FXML
    private MenuItem fileShowCountDown;
    @FXML
    private MenuItem editCancel;
    @FXML
    private Button quarterHourProgramButton;
    @FXML
    private Button halfHourProgramButton;
    @FXML
    private Button hourProgramButton;
    @FXML
    private Button twoHoursProgramButton;
    @FXML
    private Button customProgramButton;
    @FXML
    private RadioButton shutdownRadioButton;
    @FXML
    private RadioButton rebootRadioButton;
    @FXML
    private RadioButton hibernateRadioButton;
    @FXML
    private ToggleGroup scheduledModeToggleGroup;
    @FXML
    private Button cancelButton;
    @FXML
    private Button showCountDownButton;

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    private void fileNewCustomScheduledShutdown(ActionEvent event) throws IOException {
        main.showCustomScheduled();
    }

    @FXML
    void fileExitOnAction(ActionEvent event) {
        if (main.getMainController().isScheduled()) {
            Alert confirmationWindow = createConfirmationWindow();

            Optional<ButtonType> response = confirmationWindow.showAndWait();
            if (response.isPresent() && response.get() == ButtonType.OK) {
                WindowsCommandPrompt.cancel();
                main.getCountDownController().getTimer().cancel();
                Platform.exit();
            }
        } else Platform.exit();
    }

    @FXML
    private void fileShowCountDownOnAction(ActionEvent event) {
        showCountDown();
    }

    @FXML
    void editCancelOnAction(ActionEvent event) {
        cancelScheduledShutdown();
    }

    @FXML
    void helpAboutOnAction(ActionEvent event) {
        Alert about = new Alert(Alert.AlertType.INFORMATION);
        DialogPane dialogPane = about.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        Stage stage = (Stage) about.getDialogPane().getScene().getWindow();
        Image image = new Image(getClass().getResourceAsStream("/img/monitor.png"));
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(64);
        imageView.setFitWidth(64);
        stage.getIcons().add(image);
        about.setGraphic(imageView);
        about.setTitle("À propos");
        about.setHeaderText(Reference.APPLICATION_NAME + " (ver. " + Reference.APPLICATION_VERSION + "), créé en Juin 2019.");
        about.setContentText("©PILLOT Anthony. Tous droits réservés.\n\nFonctionne sous OpenJDK " + Reference.JDK_VERSION + " avec JavaFX SDK " + Reference.JAVAFX_VERSION + ".");
        about.showAndWait();
    }

    @FXML
    void quarterHourOnAction(ActionEvent event) throws IOException {
        showProgramConfirmation(15, "minutes", 900);
    }

    @FXML
    void halfHourOnAction(ActionEvent event) throws IOException {
        showProgramConfirmation(30, "minutes", 1800);
    }

    @FXML
    void hourOnAction(ActionEvent event) throws IOException {
        showProgramConfirmation(1, "heure", 3600);
    }

    @FXML
    void twoHoursOnAction(ActionEvent event) throws IOException {
        showProgramConfirmation(2, "heures", 7200);
    }

    @FXML
    void customOnAction(ActionEvent event) throws IOException {
        main.showCustomScheduled();
    }

    @FXML
    private void showCountDownOnAction(ActionEvent event) {
        showCountDown();
    }

    @FXML
    void cancelOnAction(ActionEvent event) {
        cancelScheduledShutdown();
    }

    @FXML
    void readmeOnAction(ActionEvent event) throws IOException {
        main.showReadMe();
    }

    @FXML
    void licenseOnAction(ActionEvent event) throws IOException {
        main.showLicense();
    }

    @FXML
    private void updateOnAction(ActionEvent actionEvent) {
        try {
            URI uri = new URI(Reference.SOURCEFORGE_URI);
            ExternalResource.openWebPageBrowser(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void sourceCodeOnAction(ActionEvent actionEvent) {
        try {
            URI uri = new URI(Reference.SOURCECODE_URI);
            ExternalResource.openWebPageBrowser(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void showCountDown() {
        main.getPrimaryStage().hide();
        main.getCountDownStage().show();
    }

    private void cancelScheduledShutdown() {
        WindowsCommandPrompt.cancel();
        disableSchedulingButton(false);
        disableCancelButton(true);
        setScheduled(false);
        main.getCountDownController().getTimer().cancel();
    }


    /**
     * Open a confirmation window and wait if the ButtonType.OK isPresent to launch the requested command.
     *
     * @param displayTime          Time to display in the header text of the confirmation window.
     * @param displayFormat        Format to display if it's hour, minute or second.
     * @param timeCommandInSeconds Time which will be used by the WindowsCommandPrompt class to plan a shutdown.
     */
    private void showProgramConfirmation(int displayTime, String displayFormat, int timeCommandInSeconds) throws IOException {
        main.showProgramConfirmation(displayTime, displayFormat, timeCommandInSeconds);
    }

    private Alert createConfirmationWindow() {
        Alert confirmationWindow = new Alert(Alert.AlertType.CONFIRMATION);
        DialogPane dialogPane = confirmationWindow.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        Stage stage = (Stage) confirmationWindow.getDialogPane().getScene().getWindow();
        Image image = new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(2).png"));
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(64);
        imageView.setFitWidth(64);
        stage.getIcons().add(image);
        confirmationWindow.setGraphic(imageView);
        confirmationWindow.setTitle("Fermer l'application");
        confirmationWindow.setHeaderText("Attention ! L'arrêt de l'ordinateur est programmé.");
        confirmationWindow.setContentText("Si vous quittez l'application, la programmation actuelle sera annulée.");

        confirmationWindow.initModality(Modality.APPLICATION_MODAL);
        confirmationWindow.initOwner(main.getPrimaryStage());
        return confirmationWindow;
    }

    public void showConfirmationWindowIfIsScheduled(WindowEvent windowEvent) {

        if (main.getMainController().isScheduled()) {
            Alert confirmationWindow = createConfirmationWindow();

            Optional<ButtonType> response = confirmationWindow.showAndWait();
            if (response.isPresent() && response.get() == ButtonType.OK) {
                WindowsCommandPrompt.cancel();
                main.getCountDownController().getTimer().cancel();
                Platform.exit();
            }
            if (response.isPresent() && response.get() == ButtonType.CANCEL) {
                windowEvent.consume();
                confirmationWindow.close();
            }
        } else Platform.exit();
    }

    void disableSchedulingButton(boolean bool) {
        fileNewCustom.setDisable(bool);

        quarterHourProgramButton.setDisable(bool);
        halfHourProgramButton.setDisable(bool);
        hourProgramButton.setDisable(bool);
        twoHoursProgramButton.setDisable(bool);

        customProgramButton.setDisable(bool);
    }

    void disableCancelButton(boolean bool) {
        fileShowCountDown.setDisable(bool);
        editCancel.setDisable(bool);

        showCountDownButton.setDisable(bool);

        cancelButton.setDisable(bool);
    }

    private boolean isScheduled() {
        return isScheduled;
    }

    void setScheduled(boolean scheduled) {
        isScheduled = scheduled;
    }

    public RadioButton getShutdownRadioButton() {
        return shutdownRadioButton;
    }

    public RadioButton getRebootRadioButton() {
        return rebootRadioButton;
    }

    public RadioButton getHibernateRadioButton() {
        return hibernateRadioButton;
    }

    @FXML
    void initialize() {
        assert label1 != null : "fx:id=\"label1\" was not injected: check your FXML file 'Main.fxml'.";
        assert label2 != null : "fx:id=\"label2\" was not injected: check your FXML file 'Main.fxml'.";
        assert label3 != null : "fx:id=\"label3\" was not injected: check your FXML file 'Main.fxml'.";
        assert label4 != null : "fx:id=\"label4\" was not injected: check your FXML file 'Main.fxml'.";
        assert quarterHourProgramButton != null : "fx:id=\"quarterHourProgramButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert halfHourProgramButton != null : "fx:id=\"halfHourProgramButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert hourProgramButton != null : "fx:id=\"hourProgramButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert twoHoursProgramButton != null : "fx:id=\"twoHoursProgramButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert customProgramButton != null : "fx:id=\"customProgramButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert showCountDownButton != null : "fx:id=\"showCountDownButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert shutdownRadioButton != null : "fx:id=\"shutdownRadioButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert scheduledModeToggleGroup != null : "fx:id=\"scheduledModeToggleGroup\" was not injected: check your FXML file 'Main.fxml'.";
        assert rebootRadioButton != null : "fx:id=\"rebootRadioButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert hibernateRadioButton != null : "fx:id=\"hibernateRadioButton\" was not injected: check your FXML file 'Main.fxml'.";
        assert fileNewCustom != null : "fx:id=\"fileNewCustom\" was not injected: check your FXML file 'Main.fxml'.";
        assert fileShowCountDown != null : "fx:id=\"fileShowCountDown\" was not injected: check your FXML file 'Main.fxml'.";
        assert editCancel != null : "fx:id=\"editCancel\" was not injected: check your FXML file 'Main.fxml'.";

        isScheduled = false;
        disableCancelButton(true);

        /*
         * Adding Listener :
         * @see <a href="https://stackoverflow.com/questions/26523393/radio-button-change-event-javafx">Radio Button Change Event JavaFx</a>
         *
         * Syntax : <code>ObservableValue<? extends Boolean> obs, Boolean wasPreviouslySelected, Boolean isNowSelected</code>
         */

        shutdownRadioButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean wasPreviouslySelected, Boolean isNowSelected) {
                if (isNowSelected) {
                    fileNewCustom.setText("Nouvel arrêt personnalisé");
                    label1.setText("Programmation arrêt rapide :");
                    label2.setText("Arrêt personnalisé :");
                }
            }
        });

        rebootRadioButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean wasPreviouslySelected, Boolean isNowSelected) {
                if (isNowSelected) {
                    fileNewCustom.setText("Nouveau redémarrage personnalisé");
                    label1.setText("Programmation redémarrage rapide :");
                    label2.setText("Redémarrage personnalisé :");
                }
            }
        });

        hibernateRadioButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean wasPreviouslySelected, Boolean isNowSelected) {
                if (isNowSelected) {
                    fileNewCustom.setText("Nouvelle mise en veille personnalisée");
                    label1.setText("Programmation mise en veille rapide :");
                    label2.setText("Mise en veille prolongée personnalisée :");
                }
            }
        });
    }
}
