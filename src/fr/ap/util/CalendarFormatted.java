package fr.ap.util;

import java.util.Calendar;

/**
 * <p><strong>CalendarFormatted</strong> return the different formatted value from a created Calendar.</p>
 * <hr>
 * <p><code>{@link #getHourMinuteFormatted(Calendar)}</code> (to only get hour value from a Calendar).</p>
 * <br>
 * <p><code>{@link #getDayMonthFormatted(Calendar)}</code> (to only get day and month value formatted <strong>"15/05"</strong> from a Calendar).</p>
 *
 * @author ©PILLOT Anthony.
 */

public interface CalendarFormatted {

    /**
     * <p>Return the formatted hour value only from a Calendar. Formatted to avoid dropping '0' first number.</p>
     * <br>
     * <p>Looks like : <strong>"07"</strong>.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.²
     * @return Return formatted value which be used.
     */

    static String getHourFormatted(Calendar dateToConvert) {
        int hour = dateToConvert.get(Calendar.HOUR_OF_DAY);
        return String.format("%02d", hour);
    }

    /**
     * <p>Return the formatted minute value only from a Calendar. Formatted to avoid dropping '0' first number.</p>
     * <br>
     * <p>Looks like : <strong>"15"</strong>.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.
     * @return Return formatted value which be used.
     */

    static String getMinuteFormatted(Calendar dateToConvert) {
        int minute = dateToConvert.get(Calendar.MINUTE);
        return String.format("%02d", minute);
    }

    /**
     * <p>Return the formatted hour and minute values only from a Calendar. Formatted to avoid dropping '0' first number.</p>
     * <br>
     * <p>Looks like : <strong>"07h15"</strong>.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.
     * @return Return formatted value which be used.
     */

    static String getHourMinuteFormatted(Calendar dateToConvert) {
        int hour = dateToConvert.get(Calendar.HOUR_OF_DAY);
        int minute = dateToConvert.get(Calendar.MINUTE);
        return String.format("%02dh%02d", hour, minute);
    }

    /**
     * Return the formatted day value only from a Calendar. Formatted to avoid dropping '0' first number.
     * <br>
     * <p>Looks like : <strong>"15"</strong>.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.
     * @return Return formatted value which be used.
     */

    static String getDayFormatted(Calendar dateToConvert) {
        int day = dateToConvert.get(Calendar.DAY_OF_MONTH);
        return String.format("%02d", day);
    }

    /**
     * Return the formatted month value only from a Calendar. Formatted to avoid dropping '0' first number.
     * <br>
     * <p>Looks like : <strong>"05"</strong>.<code> + 1</code> addition because the month value starting at '0'.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.
     * @return Return formatted value which be used.
     */

    static String getMonthFormatted(Calendar dateToConvert) {
        int month = dateToConvert.get(Calendar.MONTH) + 1;
        return String.format("%02d", month);
    }

    /**
     * Return the formatted day and month values only from a Calendar. Formatted to avoid dropping '0' first number.
     * <br>
     * <p>Looks like : <strong>"15/05"</strong>.<code> + 1</code> addition because the month value starting at '0'.</p>
     * <hr>
     *
     * @param dateToConvert Get the date from the chosen Calendar.
     * @return Return formatted value which be used.
     */

    static String getDayMonthFormatted(Calendar dateToConvert) {
        int day = dateToConvert.get(Calendar.DAY_OF_MONTH);
        int month = dateToConvert.get(Calendar.MONTH) + 1;
        return String.format("%02d/%02d", day, month);
    }
}
