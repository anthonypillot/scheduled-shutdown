package fr.ap.util;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public interface ExternalResource {

    /**
     * <p>Simple method to easily open a web page in the default program browser.</p>
     *
     * @param uri URI needed to open the browser.
     * @return boolean true if everything is did right.
     */
    static boolean openWebPageBrowser(URI uri) {
        boolean isOpened;
        try {
            Desktop.getDesktop().browse(uri);
            System.out.println("EVENT : Browser Web Page is opened on \"" + uri.toString() + "\"");
            isOpened = true;
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("ERROR : Failed to open browser on \"" + uri.toString() + "\"");
            isOpened = false;
        }
        return isOpened;
    }
}
