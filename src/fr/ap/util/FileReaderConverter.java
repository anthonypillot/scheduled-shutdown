package fr.ap.util;

import java.io.*;

public interface FileReaderConverter {

    private static StringBuilder getStringBuilder(String filePath) {
        File file = new File(filePath);
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(file));
            System.out.println("EVENT : Successfully charging file " + file.toString());
        } catch (FileNotFoundException e) {
            System.out.println(e);
            System.out.println("ERROR : Cannot invoke BufferedReader to charge " + file.toString());
        }

        StringBuilder wholeContent = new StringBuilder();
        String content = "";

        try {
            assert reader != null;
            while ((content = reader.readLine()) != null) {
                wholeContent.append(content).append("\n");
            }
            System.out.println("EVENT : Successfully reading file " + file.toString());
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("ERROR : during reading " + file.toString());
        }
        return wholeContent;
    }

    /**
     * <p>Simple method to charge a file and convert it into a StringBuilder.</p>
     *
     * @param filePath Path to the file to charge from resource's files.
     * @return File converted into a StringBuilder.
     */
    static StringBuilder convertFileToStringBuilder(String filePath) {
        return getStringBuilder(filePath);
    }

    /**
     * <p>Simple method to charge a file and convert it into a StringBuilder.</p>
     *
     * @param filePath Path to the file to charge from resource's files.
     * @return File converted into a StringBuilder.
     */
    static String convertFileToString(String filePath) {
        return getStringBuilder(filePath).toString();
    }
}
