package fr.ap.application;

import fr.ap.controller.*;
import fr.ap.util.CalendarFormatted;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Calendar;

public class Main extends Application {

    private Stage primaryStage;
    private Stage customScheduledStage;
    private MainController mainController;
    private Stage countDownStage;
    private CountDownController countDownController;
    private Stage programConfirmationStage;
    private ProgramConfirmationController programConfirmationController;
    private Stage licenseStage;
    private Stage readmeStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        System.out.println("EVENT : Loading " + Reference.APPLICATION_NAME + " (ver. " + Reference.APPLICATION_VERSION + ")");

        this.primaryStage = primaryStage;

        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "Main" + Reference.FXML_EXTENSION));
        BorderPane rootPane = loader.load();
        Scene primaryScene = new Scene(rootPane);
        primaryScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        primaryStage.setTitle(Reference.APPLICATION_NAME + " (ver. " + Reference.APPLICATION_VERSION + ")");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/monitor.png")));
        primaryStage.setScene(primaryScene);

        primaryStage.resizableProperty().setValue(false);

        mainController = loader.getController();
        mainController.setMain(this);

        primaryStage.setOnCloseRequest(windowEvent -> mainController.showConfirmationWindowIfIsScheduled(windowEvent));

        primaryStage.show();
    }

    public void showCustomScheduled() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "CustomScheduled" + Reference.FXML_EXTENSION));
        customScheduledStage = new Stage();
        AnchorPane customPane = loader.load();
        Scene customScene = new Scene(customPane);
        customScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        customScheduledStage.setTitle("Paramétrage personnalisé");
        customScheduledStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(8).png")));
        customScheduledStage.setScene(customScene);
        customScheduledStage.initOwner(primaryStage);
        customScheduledStage.initModality(Modality.WINDOW_MODAL);

        customScheduledStage.resizableProperty().setValue(false);

        CustomScheduledController customScheduledController = loader.getController();
        customScheduledController.setMain(this);

        customScheduledStage.showAndWait();
    }

    public void showProgramConfirmation(int displayTime, String displayFormat, int timeCommandInSeconds) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "ProgramConfirmation" + Reference.FXML_EXTENSION));
        programConfirmationStage = new Stage();
        AnchorPane programConfirmationPane = loader.load();
        Scene programConfirmationScene = new Scene(programConfirmationPane);
        programConfirmationScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        programConfirmationStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(2).png")));
        programConfirmationStage.setScene(programConfirmationScene);
        programConfirmationStage.initOwner(primaryStage);
        programConfirmationStage.initModality(Modality.WINDOW_MODAL);

        programConfirmationStage.resizableProperty().setValue(false);

        programConfirmationController = loader.getController();
        programConfirmationController.setMain(this);

        programConfirmationController.initializeVariables(displayTime, displayFormat, timeCommandInSeconds);

        Calendar timeScheduled = Calendar.getInstance();
        timeScheduled.add(Calendar.SECOND, timeCommandInSeconds);

        labelConfiguration(displayTime, displayFormat, timeScheduled);

        programConfirmationStage.showAndWait();
    }

    private void labelConfiguration(int displayTime, String displayFormat, Calendar timeScheduled) {
        if (displayTime == 0) {
            if (mainController.getShutdownRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation de l'arrêt");
                programConfirmationController.getHeaderLabel().setText("Confirmation arrêt de l'ordinateur.");
                programConfirmationController.getContentLabel().setText("Votre ordinateur s'arrêtera à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
            if (mainController.getRebootRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation du redémarrage");
                programConfirmationController.getHeaderLabel().setText("Confirmation redémarrage de l'ordinateur.");
                programConfirmationController.getContentLabel().setText("Votre ordinateur redémarrera à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
            if (mainController.getHibernateRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation de la mise en veille prolongée");
                programConfirmationController.getHeaderLabel().setText("Confirmation mise en veille prolongée de l'ordinateur.");
                programConfirmationController.getContentLabel().setText("Votre ordinateur se mettra en veille à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
        } else {
            if (mainController.getShutdownRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation de l'arrêt");
                programConfirmationController.getHeaderLabel().setText("Confirmation arrêt de l'ordinateur dans " + displayTime + " " + displayFormat + ".");
                programConfirmationController.getContentLabel().setText("Votre ordinateur s'arrêtera à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
            if (mainController.getRebootRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation du redémarrage");
                programConfirmationController.getHeaderLabel().setText("Confirmation redémarrage de l'ordinateur dans " + displayTime + " " + displayFormat + ".");
                programConfirmationController.getContentLabel().setText("Votre ordinateur redémarrera à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
            if (mainController.getHibernateRadioButton().isSelected()) {
                programConfirmationStage.setTitle("Confirmation de la mise en veille prolongée");
                programConfirmationController.getHeaderLabel().setText("Confirmation mise en veille prolongée de l'ordinateur dans " + displayTime + " " + displayFormat + ".");
                programConfirmationController.getContentLabel().setText("Votre ordinateur se mettra en veille à " + CalendarFormatted.getHourMinuteFormatted(timeScheduled) + ", le " + CalendarFormatted.getDayMonthFormatted(timeScheduled) + ".");
            }
        }
    }

    public void showCountDownWindow(int timeScheduled) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "CountDown" + Reference.FXML_EXTENSION));
        countDownStage = new Stage();
        AnchorPane countDownPane = loader.load();
        Scene countDownScene = new Scene(countDownPane);
        countDownScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        countDownStage.setTitle("Compte à rebours ...");
        countDownStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/chrono/stopwatch.png")));
        countDownStage.setScene(countDownScene);
        countDownStage.initOwner(primaryStage);
        countDownStage.initModality(Modality.WINDOW_MODAL);

        countDownStage.resizableProperty().setValue(false);

        countDownController = loader.getController();
        countDownController.setMain(this);

        if (mainController.getShutdownRadioButton().isSelected()) {
            countDownController.getHeadLabel().setText("Votre ordinateur s'arrêtera dans :");
        }
        if (mainController.getRebootRadioButton().isSelected()) {
            countDownController.getHeadLabel().setText("Votre ordinateur redémarrera dans :");
        }
        if (mainController.getHibernateRadioButton().isSelected()) {
            countDownController.getHeadLabel().setText("Votre ordinateur se mettra en veille dans :");
        }

        countDownController.getCountDownLabel().setText(countDownController.timeScheduledToString(timeScheduled));
        countDownController.launchCountDown(timeScheduled);

        countDownStage.setOnCloseRequest(windowEvent -> countDownController.displayConfirmationWindow(windowEvent));
    }

    public void showReadMe() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "ReadMe" + Reference.FXML_EXTENSION));
        readmeStage = new Stage();
        AnchorPane countDownPane = loader.load();
        Scene licenseScene = new Scene(countDownPane);
        licenseScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        readmeStage.setTitle("Lisez-moi");
        readmeStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(4).png")));
        readmeStage.setScene(licenseScene);

        readmeStage.initOwner(primaryStage);
        readmeStage.initModality(Modality.WINDOW_MODAL);

        readmeStage.resizableProperty().setValue(false);

        ReadMeController readmeController = loader.getController();
        readmeController.setMain(this);

        readmeStage.show();
    }

    public void showLicense() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Reference.FXML_PATH + "License" + Reference.FXML_EXTENSION));
        licenseStage = new Stage();
        AnchorPane countDownPane = loader.load();
        Scene licenseScene = new Scene(countDownPane);
        licenseScene.getStylesheets().add(getClass().getResource(Reference.CSS_FULL_PATH).toExternalForm());
        licenseStage.setTitle("Contrat de Licence Utilisateur Final");
        licenseStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/stopwatch/stopwatch(4).png")));
        licenseStage.setScene(licenseScene);

        licenseStage.initOwner(primaryStage);
        licenseStage.initModality(Modality.WINDOW_MODAL);

        licenseStage.resizableProperty().setValue(false);

        LicenseController licenseController = loader.getController();
        licenseController.setMain(this);

        licenseStage.show();
    }

    public MainController getMainController() {
        return mainController;
    }

    public CountDownController getCountDownController() {
        return countDownController;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Stage getCustomScheduledStage() {
        return customScheduledStage;
    }

    public Stage getProgramConfirmationStage() {
        return programConfirmationStage;
    }

    public Stage getCountDownStage() {
        return countDownStage;
    }

    public Stage getReadmeStage() {
        return readmeStage;
    }

    public Stage getLicenseStage() {
        return licenseStage;
    }
}
