package fr.ap.application;

public abstract class Reference {

    private Reference() {
    }

    private static final String CSS_FILENAME = "JMetroLightTheme";
    private static final String CSS_PATH = "/CSS/";

    private static final String CSS_EXTENSION = ".css";

    public static final String CSS_FULL_PATH = Reference.CSS_PATH + Reference.CSS_FILENAME + Reference.CSS_EXTENSION;

    public static final String FXML_PATH = "/FXML/";

    public static final String FXML_EXTENSION = ".fxml";
    public static final String APPLICATION_NAME = "Scheduled-Shutdown";
    public static final String APPLICATION_VERSION = "beta 0.8.x";
    public static final String SOURCEFORGE_URI = "https://sourceforge.net/projects/scheduled-shutdown/";
    public static final String SOURCECODE_URI = "https://gitlab.com/RAIIIIIN/scheduled-shutdown";

    public static final String JDK_VERSION = "13.0.1";
    public static final String JAVAFX_VERSION = "13.0.1";
}
