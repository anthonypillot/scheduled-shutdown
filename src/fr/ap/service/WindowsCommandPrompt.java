package fr.ap.service;

import fr.ap.application.Reference;

import java.io.IOException;
import java.io.InputStream;

/**
 * <p>WindowsCommandPrompt use Process, Runtime and InputStream to send prompt command line to the OS.</p>
 * <hr>
 * <p>Send a console message if the operation failed.</p>
 *
 * @author ©PILLOT Anthony.
 */

public abstract class WindowsCommandPrompt {

    private static final String SHUTDOWN_COMMENTARY_MESSAGE = "\"" + Reference.APPLICATION_NAME + " a programmé l'arrêt de votre ordinateur.\"";
    private static final String REBOOT_COMMENTARY_MESSAGE = "\"" + Reference.APPLICATION_NAME + " a programmé le redémarrage de votre ordinateur.\"";

    private static final String ERROR_MESSAGE = "ERROR : Windows command not sent | ";

    private WindowsCommandPrompt() {
    }

    /**
     * <p>Send a Windows OS command to scheduled a shutdown of Windows, looking like :</p>
     * <p><code>shutdown -s -t 'seconds' -c "COMMENTARY_MESSAGE"</code>seconds</p>
     * <p>Advise by console message if something get wrong, like "<code>EVENT :</code>" or "<code>ERROR :</code>".</p>
     * <hr>
     *
     * @param seconds integer will be passed in the Windows command.
     */
    public static void shutdown(int seconds) {
        try {
            Process process = Runtime.getRuntime().exec("shutdown -s -t " + seconds + " -c " + SHUTDOWN_COMMENTARY_MESSAGE);
            InputStream inputStream = process.getInputStream();
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
            System.out.println("EVENT : Windows command sent : shutdown -s -t " + seconds);
        } catch (IOException error) {
            System.out.println(ERROR_MESSAGE + error);
        }
    }

    /**
     * <p>Send a Windows OS command to scheduled a reboot of Windows, looking like :</p>
     * <p><code>shutdown -g -t 'seconds' -c "COMMENTARY_MESSAGE"</code>seconds</p>
     * <p>Advise by console message if something get wrong, like "<code>EVENT :</code>" or "<code>ERROR :</code>".</p>
     * <hr>
     *
     * @param seconds integer will be passed in the Windows command.
     */
    public static void reboot(int seconds) {
        try {
            Process process = Runtime.getRuntime().exec("shutdown -g -t " + seconds + " -c " + REBOOT_COMMENTARY_MESSAGE);
            InputStream inputStream = process.getInputStream();
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
            System.out.println("EVENT : Windows command sent : shutdown -g -t " + seconds);
        } catch (IOException error) {
            System.out.println(ERROR_MESSAGE + error);
        }
    }

    /**
     * <p>Send a Windows OS command to scheduled a hibernate of Windows, looking like :</p>
     * <p><code>shutdown -h -t 'seconds' -c "COMMENTARY_MESSAGE"</code>seconds</p>
     * <p>Advise by console message if something get wrong, like "<code>EVENT :</code>" or "<code>ERROR :</code>".</p>
     */
    public static void hibernate() {
        try {
            Process process = Runtime.getRuntime().exec("shutdown -h");
            InputStream inputStream = process.getInputStream();
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
            System.out.println("EVENT : Windows command sent : shutdown -h");
        } catch (IOException error) {
            System.out.println(ERROR_MESSAGE + error);
        }
    }

    /**
     * <p>Send a Windows OS command to abort any scheduled shutdown, looking like :</p>
     * <p><code>shutdown -a</code></p>
     * <p>Advise by console message if something get wrong, like "<code>EVENT :</code>" or "<code>ERROR :</code>".</p>
     */
    public static void cancel() {
        try {
            Process process = Runtime.getRuntime().exec("shutdown -a");
            InputStream inputStream = process.getInputStream();
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
            System.out.println("EVENT : Windows command sent : shutdown -a ");
            System.out.println("EVENT : Abandonment of programming");
        } catch (IOException error) {
            System.out.println(ERROR_MESSAGE + error);
        }
    }
}
