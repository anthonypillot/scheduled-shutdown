**Scheduled-Shutdown (beta)** *(actual ver. 0.8)*
=

*Source code language : english. Desktop application display language : french <code>(english translation in progress)</code>.*

Java Runtime Environment (JRE) : **[OpenJDK-13.0.1](https://jdk.java.net/)**.

Dependencies : **[JavaFX-13.0.1](https://openjfx.io/)**. Scheduled-Shutdown is running with a [Custom JDK+JavaFX image](https://openjfx.io/openjfx-docs/#modular) created with <code>jlink</code>.
___

**Scheduled-Shutdown** is a micro-program which can plan a shutdown of the *(Windows only)* computer with an user friendly graphic interface by executing directly a shell command in the operating system.

___

***Incoming update xx.xx.2019 (ver. 0.x)*** : *(I'm nowhere near to be on this update)* macOS compatibility.

***Incoming update xx.xx.2019 (ver. 0.x)*** : Add a date picker next to the custom scheduled selection.

***Update 12.01.2019 (ver. 0.8) CHANGELOG*** :
-
JRE update to the **OpenJDK-13.0.1** version. JavaFX Updated to the **13.0.1** version. Scheduled-Shutdown is now under the GNU V3 License.
Various source code changes. See different commits. on GitLab.

***Update 07.19.2019 (ver. 0.7.3) CHANGELOG*** :
-
JRE update to the **OpenJDK-12.0.2** version. Graphical update. Addition of License and Readme window.

***Update 07.19.2019 (ver. 0.7.2) CHANGELOG*** :
-
General minor bug fixes and performance improvements.

***Update 07.16.2019 (ver. 0.7) CHANGELOG :***
-
Reboot and Hibernate functions implement by a Radio Button on the Main window.

***Bonus update 07.05.2019 (ver. 0.6) CHANGELOG :***
-
Graphical update, pixeled images cleaned, checkbox countdown added and global improvement (debug, clean code).

***Update 06.24.2019 (ver. 0.5) CHANGELOG :***
-
Add countdown when a scheduled shutdown is placed. User can switch between application's main view and countdown window by a one trick button.

![presentation.png](https://gitlab.com/RAIIIIIN/scheduled-shutdown/raw/master/resources/extra/presentation.png)