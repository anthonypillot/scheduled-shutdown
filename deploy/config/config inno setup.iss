; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Scheduled-Shutdown"
#define MyAppVersion "0.7.3"
#define MyAppPublisher "(C)PILLOT Anthony"
#define MyAppURL "https://gitlab.com/RAIIIIIN"
#define MyAppExeName "scheduled-shutdown-0.7.3.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
; NOTE: How to configure Signtool : https://stackoverflow.com/questions/19160779/innosetup-code-signing-certificate
SignTool=MicrosoftSigntool $f
AppId={{742E1B4B-C1B6-49E4-BDCC-B2FD5BC042B5}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
LicenseFile=D:\DevOps\Java-Workspace\scheduled-shutdown-deploy\portable\LICENSE.txt
InfoAfterFile=D:\DevOps\Java-Workspace\scheduled-shutdown-deploy\portable\README.txt
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
OutputDir=D:\DevOps\Java-Workspace\scheduled-shutdown-deploy
OutputBaseFilename=scheduled-shutdown-0.7.3
SetupIconFile=D:\DevOps\Java-Workspace\scheduled-shutdown-deploy\monitor_icon.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "french"; MessagesFile: "compiler:Languages\French.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "D:\DevOps\Java-Workspace\scheduled-shutdown-deploy\portable\scheduled-shutdown-0.7.3.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\DevOps\Java-Workspace\scheduled-shutdown-deploy\portable\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

